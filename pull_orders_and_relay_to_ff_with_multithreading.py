import threading, time, logging
import csv
import MySQLdb
import datetime
import requests
import json
import sys
from requests.auth import HTTPBasicAuth

CONCURRENCY_RATE = 3
BATCH_SIZE = 3

logging.basicConfig(level=logging.DEBUG)
logging.basicConfig(filename='app.log', filemode='w', format='%(name)s - %(levelname)s - %(message)s')


class MyThread(threading.Thread):
    def __init__(self, threadName, concurrencyFactor, dataInput, batchSize, offset):
        threading.Thread.__init__(self)
        self.ThreadName = threadName
        self.concurrencyFactor = concurrencyFactor
        self.dataInput = dataInput
        self.batchSize = batchSize
        self.offset = offset

    def run(self):
        logging.info("Inside run for %s " % self.ThreadName)
        self.invoke()
        logging.info("Running code for %s " % self.concurrencyFactor)
        logging.info("exiting for %s " % self.ThreadName)

    def callRelayMethod(self, args):
        for i in range(0, len(args), 3):
            payload = json.dumps({
            "order_ids" : args[i:i+3],
            "exchange_name": "swiggy.orders"
            })
            print payload
            url = 'http://checkout-service1.swiggy.prod/api/v1/order/correct'
            r = requests.post(url, data=payload, auth=HTTPBasicAuth('ozlRZTueR2kqv7gm', 'd2f2bd530ab25fef47ab220d0e0aa108'), headers={'Content-Type':'application/json'})
            print r.text

    def invoke(self):
        filteredList = [ self.dataInput[i] for i in xrange(len(self.dataInput)) if i%self.concurrencyFactor == self.offset-1]
        print(filteredList)
        args = list()
        for item in filteredList:
            args.append(item)
            if len(args) == self.batchSize:
                logging.info("%s %s"% (self.ThreadName, args))
                self.callRelayMethod(args)
                args = list()
        if len(args) > 0:
            logging.info("%s %s" % (self.ThreadName, args))
            self.callRelayMethod(args)

def main():

    db = MySQLdb.connect(host="checkout-p-db-slave-01.swiggyops.de",  # your host
                        user="swiggy",  # username
                        passwd="swiggy@2014",  # password
                        db="swiggy_order_management")

    startTime = sys.argv[1]+ " " + sys.argv[2]
    endTime= sys.argv[3]+ " "+ sys.argv[4]
    cursor = db.cursor()
    order_not_like_subscription = '\"orderType\":\"subscription\"'
    query = """select id from `swiggy_orders` 
              where  `created_on` > '%s' 
              and `created_on` < '%s' 
              and `order_details` not like %s;"""  %(startTime, endTime, "'%" +order_not_like_subscription+"%'")
    print query
    cursor.execute(query)

    list_order = []
    for row in cursor.fetchall():
       list_order.append(row[0])
       print row[0]
    db.close()

    data = list(xrange(10))
    workers = list()
    for i in xrange(CONCURRENCY_RATE):
        threadName = "Thread-%d"%i
        worker = MyThread(threadName=threadName, concurrencyFactor=CONCURRENCY_RATE, dataInput=list_order, batchSize=BATCH_SIZE, offset=i+1)
        worker.start()
        workers.append(worker)

    time.sleep(3)
    for worker in workers:
        worker.join()
    logging.info("Finish")


if __name__ == '__main__':
    main()
